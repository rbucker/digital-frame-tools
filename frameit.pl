my $numArgs = $#ARGV + 1;
($numArgs>1) || die "USAGE: frameit.pl src... dst";
my ($src, $dest) = @ARGV;

my $range = 26;

$i=0;
@files=`ls -1 "${src}"`;
chdir($src) or die "$!";
foreach $a  (@files) {
   chomp $a;
   #$t = sprintf("/Users/Shared/DigitalFrame/out/%cP090111-%03d.jpg",ord('A')+int(rand($range)),$i);
   $t = sprintf("%s/P171114-%03d.jpg", $dest, $i);
   $w = "magick convert '$a' -geometry 600x600 \"$t\"";
   print "$w\n";
   `$w`;
   $i++;
}